import * as React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import {TextField} from 'formik-material-ui';
import {Formik, Form, Field} from 'formik';
import * as Yup from 'yup';

const styles = {};

const schema = Yup.object().shape({
  email: Yup.string()
    .email()
    .required('Required'),
  password: Yup.string().required('Required'),
});

interface Props extends RouteComponentProps {
  handleSubmit: (opts: {email: string; password: string}) => Promise<void>;
}
interface Values {
  email: string;
  password: string;
}

const Login = (props: Props) => (
  <div style={{padding: 10, flex: 1}}>
    <Paper style={{padding: 20}} elevation={4}>
      <Typography variant="headline" component="h3">
        Login
      </Typography>
      <Formik<Values>
        onSubmit={async (values, {setSubmitting, setStatus}) => {
          setSubmitting(true);

          try {
            await props.handleSubmit(values);
            setSubmitting(false);
            props.history.push(
              props.location.state ? props.location.state.from : '/'
            );
          } catch (ex) {
            setStatus(ex.message);
            setSubmitting(false);
          }
        }}
        validationSchema={schema}
        initialValues={{email: '', password: ''}}
        render={({handleSubmit, status}) => (
          <Form>
            <Field name="email" label="Email" component={TextField} />
            <br />
            <Field
              name="password"
              label="Password"
              type="password"
              component={TextField}
            />
            <br />
            <Button
              variant="contained"
              color="primary"
              style={{margin: 10}}
              onClick={() => handleSubmit()}
            >
              Login
            </Button>
            {status && <Typography>{status}</Typography>}
          </Form>
        )}
      />
    </Paper>
  </div>
);

export default withStyles(styles)(Login);
