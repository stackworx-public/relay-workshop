import * as React from 'react';
import {WithStyles, createStyles, withStyles} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import {Theme} from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import {withRouter, RouteComponentProps} from 'react-router-dom';

import Sidebar from './Sidebar';
import {AuthContext, AuthContextType} from '../contexts';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    container: {
      padding: 10,
    },
  });

interface Props extends WithStyles<typeof styles>, RouteComponentProps {
  children: React.ReactChild;
  handleLogout: () => void;
}

interface State {
  open: boolean;
}

class Root extends React.Component<Props, State> {
  public state = {open: false};

  public toggleDrawer = (open: boolean) => () => {
    this.setState({open});
  };

  public render() {
    const {children, classes, handleLogout} = this.props;
    const {open} = this.state;

    return (
      <AuthContext.Consumer>
        {(auth: AuthContextType) => (
          <div className={classes.root}>
            {auth.authenticated && (
              <AppBar position="static">
                <Toolbar>
                  <IconButton
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="Menu"
                  >
                    <MenuIcon
                      onClick={() =>
                        this.setState((state) => ({
                          open: !state.open,
                        }))
                      }
                    />
                  </IconButton>
                  <Typography
                    variant="h6"
                    color="inherit"
                    className={classes.grow}
                  >
                    Relay Workshop
                  </Typography>
                  {/* TODO: differnt menu for authed vs unauthed */}
                  <Button color="inherit">{auth.username}</Button>
                </Toolbar>
              </AppBar>
            )}
            {auth.authenticated && (
              <Sidebar
                toggleDrawer={this.toggleDrawer}
                open={open}
                handleLogout={handleLogout}
              />
            )}
            <div className={classes.container}>{children}</div>
          </div>
        )}
      </AuthContext.Consumer>
    );
  }
}

export default withStyles(styles)(withRouter(Root));
