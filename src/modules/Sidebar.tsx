import React from 'react';
import {WithStyles, createStyles, withStyles} from '@material-ui/core';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = createStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

interface Props extends WithStyles<typeof styles> {
  toggleDrawer: (open: boolean) => () => void;
  open: boolean;
  handleLogout: () => void;
}

class Sidebar extends React.Component<Props> {
  public render() {
    const {classes, toggleDrawer, open, handleLogout} = this.props;

    const sideList = (
      <div className={classes.list}>
        <List>
          {['Home', 'Companies', 'Users'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          <ListItem button>
            <ListItemText primary="Logout" onClick={() => handleLogout()} />
          </ListItem>
        </List>
      </div>
    );

    return (
      <Drawer open={open} onClose={toggleDrawer(false)}>
        <div
          tabIndex={0}
          role="button"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
        >
          {sideList}
        </div>
      </Drawer>
    );
  }
}

export default withStyles(styles)(Sidebar);
