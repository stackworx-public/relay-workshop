import * as React from 'react';
// import {Environment, Network, RecordSource, Store} from 'relay-runtime';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  RouteComponentProps,
  RouteProps,
  Switch,
} from 'react-router-dom';
import {LoginService, ConfigService} from '@stackworx/react';
import {createMuiTheme} from '@material-ui/core';
import {MuiThemeProvider} from '@material-ui/core/styles';

import Root from './modules/Root';
import Home from './modules/Home';
import Login from './modules/Login';

const NotFound = () => <div>Not Found</div>;

import {
  AuthContext,
  AuthContextType,
  // RelayContext,
  // RelayContextType,
} from './contexts';

// const loginService = new LoginService(ConfigService.serverUri);
const loginService = new LoginService(
  'https://api.metcon.dev.stackworx.cloud',
  (v: string) => {
    return btoa(v);
  }
);

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
});

function PrivateRoute({
  // tslint:disable-next-line
  component: Component,
  ...rest
}: RouteProps) {
  return (
    <AuthContext.Consumer>
      {(auth) => (
        <Route
          {...rest}
          render={(props: any) =>
            auth.authenticated ? (
              // @ts-ignore
              <Component {...props} />
            ) : (
              <Redirect
                to={{
                  pathname: '/login',
                  state: {from: props.location},
                }}
              />
            )
          }
        />
      )}
    </AuthContext.Consumer>
  );
}

interface State {
  auth: AuthContextType;
  loading: boolean;
  // environment: RelayContextType;
}

export default class App extends React.Component<{}, State> {
  constructor(props: {}) {
    super(props);

    this.state = {
      auth: {authenticated: false},
      loading: true,
      // environment: this.createRelayEnvironment(),
    };
  }

  /*
  createRelayEnvironment() {
    const source = new RecordSource();
    const store = new Store(source);
    const network = Network.create();
    const handlerProvider = null;

    const environment = new Environment({
      handlerProvider, // Can omit.
      network,
      store,
    });

    return environment;
  }
  */

  public handleLogout = () => {
    localStorage.removeItem('auth');
    this.setState({auth: {authenticated: false}});
  };

  public componentDidMount() {
    // TODO: yup schema check auth
    const authString = localStorage.getItem('auth');

    if (authString) {
      const auth = JSON.parse(authString);

      if (auth) {
        this.setState({auth, loading: false});
        return;
      }
    }

    this.setState({loading: false});
  }

  public handleSubmit = async ({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): Promise<void> => {
    const {access_token} = await loginService.login(email, password);

    const auth: AuthContextType = {
      authenticated: true,
      token: access_token,
      username: email,
    };

    this.setState(
      {
        auth,
      },
      () => {
        localStorage.setItem('auth', JSON.stringify(auth));
      }
    );
  };

  public render() {
    const {auth, loading} = this.state;

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <MuiThemeProvider theme={theme}>
        <AuthContext.Provider value={auth}>
          {/* <RelayContext.Provider value={environment}> */}
          <Router>
            <Root handleLogout={this.handleLogout}>
              <Switch>
                <PrivateRoute path="/" exact component={Home} />
                <Route
                  path="/login/"
                  extact
                  component={(props: RouteComponentProps) => (
                    <Login {...props} handleSubmit={this.handleSubmit} />
                  )}
                />
                <Route component={NotFound} />
              </Switch>
            </Root>
          </Router>
          {/* </RelayContext.Provider> */}
        </AuthContext.Provider>
      </MuiThemeProvider>
    );
  }
}
